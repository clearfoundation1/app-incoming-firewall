<?php

$lang['incoming_firewall_allowed_incoming_connections'] = 'Дозволені вхідні підключення';
$lang['incoming_firewall_app_description'] = 'Додаток «Вхідний брандмауер» захищає від поганих хлопців, обмежуючи доступ до вашої системи та блокуючи небажані з’єднання.';
$lang['incoming_firewall_app_name'] = 'Вхідний брандмауер';
$lang['incoming_firewall_blocked_incoming_connections'] = 'Заблоковані вхідні з’єднання';
